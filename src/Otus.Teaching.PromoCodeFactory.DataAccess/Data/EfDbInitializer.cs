﻿using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer(DataContext dataContext) : IDbInitializer
    {
        public void InitializeDb()
        {
            if (!dataContext.Employees.Any())
            {
                dataContext.AddRange(FakeDataFactory.Employees);
                dataContext.SaveChanges();
            }

            if (!dataContext.Preferences.Any())
            {
                dataContext.AddRange(FakeDataFactory.Preferences);
                dataContext.SaveChanges();
            }

            if (dataContext.Customers.Any()) return;
            dataContext.AddRange(FakeDataFactory.Customers);
            dataContext.SaveChanges();
        }
    }
}